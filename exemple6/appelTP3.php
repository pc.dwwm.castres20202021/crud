<?php
	// pour afficher le code (utile pour le cours !!)
	// echo '<h1>CODE PHP</h1>';
	// highlight_file('appelTP3.php');
	echo '<h1>RESULTATS</h1>';

	if (!isset($_GET['realisateur']) || !isset($_GET['titre']) || $_GET['realisateur']=='' || $_GET['titre']==''  ){
		echo '<br/> Vous n\'avez pas saisi tous les paramètres';
		return;
	}
	
	include("connexion.php");
	$bdd=connexionBD('cinema');	

	$reqSQL='UPDATE films SET annee=:annee WHERE titre = :titre AND realisateur = :realisateur';

	echo '<h2>' .$reqSQL. '</h2>';
	echo '('.$_GET['titre'].', ' .$_GET['realisateur'].', ' .$_GET['annee'].')';
	$requete=$bdd->prepare($reqSQL);
	$resultat=$requete->execute(array(
		'titre'=>$_GET['titre'], 
		'realisateur'=>$_GET['realisateur'],
		'annee'=>$_GET['annee']
	));

	/* pour tester le résultat : 0 si pas de DELETE */
	if($requete->rowCount() ){
		echo '<br/> L\'UPDATE a été effectué ' .$requete->rowCount(). ' fois';
	} else {
		echo '<br/> L\'UPDATE a échoué';
	}
	$requete->closeCursor();
?>
