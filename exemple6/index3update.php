<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Test de UPDATE de l'élément INSERTé dans films</title>
	</head>
	
	<body>
		<h1>Test d'un UPDATE de l'élément INSERTé dans films</h1>
		<form action="appelTP3.php" method="GET" >
			<p><label for="titre">titre du film cherché : </label>
			<input type="text" name="titre" id="titre" placeholder="titre"></p>
			<p><label for="realisateur">realisateur du film cherché : </label>
			<input type="text" name="realisateur" id="realisateur" placeholder="realisateur"></p>
			<p><label for="annee">année à modifier : </label>
			<input type="text" name="annee" id="annee" placeholder="annee"></p>
		  <p><input type="submit" value="Valider"></p>
		</form>	
	</body>	
</html>