<?php
	// pour afficher le code (utile pour le cours !!)
	//echo '<h1>CODE PHP</h1>';
	//highlight_file('appelTP1.php');
	echo '<h1>RESULTATS</h1>';

	if (isset($_GET['realisateur'], $_GET['titre'], $_GET['annee'])){
		include("connexion.php");
		$bdd=connexionBD('cinema');	
		
		$reqSQL='INSERT INTO films (titre, realisateur, annee) 
		VALUES (:titre, :realisateur, :annee) ';
		//('L\'homme irrationnel','Woody Allen','2015','americain', '1h35');

		echo '<h2>' .$reqSQL. '</h2>';
		echo '('.$_GET['titre'].', ' .$_GET['realisateur'].', ' .$_GET['annee'].')';
		$requete=$bdd->prepare($reqSQL);
		$resultat=$requete->execute(array(
			'titre'=>$_GET['titre'], 
			'realisateur'=>$_GET['realisateur'], 
			'annee'=>$_GET['annee']
		));

		/* pour tester le résultat : 0 si échec */ 
		if($resultat){
			echo '<br/>Le INSERT a été effectuée';
		} else {
			echo '<br/>Le INSERT a échouée';
		}
		//On libère la requète
		$requete->closeCursor();
	}
?>
