<?php
	// pour afficher le code (utile pour le cours !!)
	//echo '<h1>CODE PHP</h1>';
	//highlight_file(basename(__FILE__));
	echo '<h1>Principes de l\'exercice</h1>';
	echo '
		Test de Select "en dur"
		<br> Where realisateur like ?
		<br> execute (%manki%) en dur
	';
	
	echo '<h1>Résultats</h1>';

	// connexion à la BD
	include("connexion.php");
	$bdd=connexionBD('cinema');		

	// reqSQL : version avec ? qui sera passé en dur
	$reqSQL='SELECT * FROM films WHERE realisateur like ? order by realisateur, annee';
	echo $reqSQL.'<br/>';

	// prepare et execute : version avec ? dans le reqSQL
	$requete=$bdd->prepare($reqSQL);
	$requete->execute(array('%manki%')); //'manki%' prend la place du ? dans $reqSQL

	// affichage des résultats	
	echo '<h2>  Affichage des résultats</h2>';
	print_r($requete);
	$cpt=1;
	echo '<ul>';
	while ($ligne = $requete->fetch()) {
		echo '<li>[' .$cpt++. '] ' .$ligne['realisateur']. ' : ' .$ligne['annee']. ' - ' .$ligne['titre']. '</li>';
	}
	echo '</ul>';

	// fermeture préférable pour exécuter une nouvelle requete
	$requete->closeCursor();
	echo '<h1>Fin</h1>';
?>
