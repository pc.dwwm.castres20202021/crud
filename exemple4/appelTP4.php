<?php
	echo'POST : ';print_r($_POST);echo'<br/>';
	echo'GET : ';print_r($_GET);echo'<br/>';
	echo'URL : ';print_r($_SERVER['PHP_SELF']);echo'<br/>';  
	echo '<h1>Principes de l\'exercice</h1>';
	echo '
		Formulaire de saisie. Methode  GET.  
		<br>Where realisateur like ?  and > ?
		<br>Faille XSS gérée via un prepare() execute().
		<br>
	';
	print_r($_GET);
	echo '<h1>Résultats</h1>';

	// connexion à la BD
	include("connexion.php");
	$bdd=connexionBD('cinema');	

	// reqSQL : version avec :realisateur au lieu de ?		
	echo '<h2>  On peut tester directement dans la ligne d\'URL</h2>';
	$reqSQL='SELECT * FROM films WHERE realisateur like ? and annee < ? order by realisateur, annee';
	echo $reqSQL.'<br/>';

	// prepare et execute : version ? dans le reqSQL	
	$requete=$bdd->prepare($reqSQL);
	$requete->execute(array($_GET['realisateur'], $_GET['annee']));

	// affichage des résultats
	print_r($requete);
	$cpt=1;
	echo '<ul>';
	$lignes = $requete->fetchAll();
	
	// pour tester : on encode le fetch all en JSON
	$lignesJSON=json_encode($lignes);
	print_r ($lignesJSON);

	foreach ($lignes as $ligne) {
		echo '<li>[==' .$cpt++. '] ' .$ligne['realisateur']. ' : ' .$ligne['annee']. ' - ' .$ligne['titre']. '</li>';
	}
	echo '</ul>';
	if($cpt==1) echo '<h3>aucun tuples trouvés<h3>';
	
	// fermeture préférable pour exécuter une nouvelle requete
	$requete->closeCursor();

	echo '<h1>Fin</h1>';
?>
