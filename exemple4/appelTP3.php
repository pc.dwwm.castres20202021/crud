<?php
	echo'POST : ';print_r($_POST);echo'<br/>';
	echo'GET : ';print_r($_GET);echo'<br/>';
	echo'URL : ';print_r($_SERVER['PHP_SELF']);echo'<br/>';  
	echo '<h1>Principes de l\'exercice</h1>';
	echo '
		Formulaire de saisie. Methode  GET.  
		Where realisateur = ?
		<br>Faille XSS gérée via un prepare() execute().
	';
	
	echo '<h1>Résultats</h1>';
	// connexion à la BD
	include("connexion.php");
	$bdd=connexionBD('cinema');	

	// reqSQL : version avec ? le reqSQL : pas faille XSS
	echo '<h2> Pas de faille XSS en saisissant exactement  : Woody Allen\' or \'a\'=\'a</h2>';
	$reqSQL='SELECT * FROM films WHERE realisateur = ? order by realisateur, annee';
	echo $reqSQL.'<br/>';

	// prepare et execute : version avec ? dans le reqSQL
	$requete=$bdd->prepare($reqSQL);
	$requete->execute(array(
		$_GET['realisateur']
	));

	// affichage des résultats
    echo '<h2>  Affichage des résultats</h2>';
	print_r($requete);
	$cpt=1;
	echo '<ul>';
	while ($ligne = $requete->fetch()) {
		echo '<li>[' .$cpt++. '] ' .$ligne['realisateur']. ' : ' .$ligne['annee']. ' - ' .$ligne['titre']. '</li>';
	}
	echo '</ul>';
	if($cpt==1) echo '<h3>aucun tuples trouvés<h3>';

	// fermeture préférable pour exécuter une nouvelle requete
	$requete->closeCursor();
	echo '<h1>Fin</h1>';
?>
